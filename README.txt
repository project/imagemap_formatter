This module is heavily based on image_link_formatter.

To use this module:

1. Add an image field and a long_text field to an entity. The image field is
   for the image, the long_text field is for the map code.

2. Edit the display settings for the entity.
   - Set the map code to hidden.
   - Set the image formatter to Image Map.
   - Choose the long_text field for the map code.

3. Add some content. The map code should only include HTML within the <map>
   tags. Usually it will only include a series of <area> tags.

For responsive image maps simply install the responsive_imagemaps module.
